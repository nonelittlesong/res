# Web IDE

和 [CI configuration file](https://docs.gitlab.com/ee/ci/yaml/README.html) 很相似，但有以下限制：

- 不能定义全局块（如，`before_script` 或 `after_script`）
- 只能添加名为 `terminal` 的作业
- 只能用以下关键字配置作业：
  - `image`
  - `services`
  - `tags`
  - `before_script`
  - `script`
  - `variables`
- 为了连接上终端，需要保持 `terminal` 作业持续运行。默认 `sleep 60` 给予 Web IDE 充分的时间连接。

**注意**，终端作业依赖于分支。
